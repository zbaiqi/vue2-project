# DataPulse数据中台前端

## 文件结构
* log 日志管理
* login 用户登录注册
* task 任务调度
* user 用户管理
* warehouse 数据仓库
* home 主页

## 特殊文件
* util 工具包（存放时间戳转日期函数等工具）
* store vuex全局状态管理文件