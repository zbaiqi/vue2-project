import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    count: 0,
    userAvatar: localStorage.getItem('avatarUrl'),
    selectedTables: [],
    source_name: '',
    database_name:'',
    table_name:'',
    resource:'已存在表',
    table_list:[],
    save_name:'任务1',
    flag: false,
   
    logsData: [], // 存储所有用户的日志数据
  },
  //操作全局变量的方法
  mutations: {
    setAvatar(state, newval) {
      state.userAvatar = newval
      localStorage.setItem('avatarUrl',newval)
    },
    setflag(state, payload) {
      state.flag = payload;
      console.log('flag:', state.flag)
    },
    setTables(state, payload) {
      state.selectedTables = payload;
      console.log('selectedTables:', state.selectedTables)
    },
    setSourceName(state, payload) {
      state.source_name = payload;
      console.log('source_name:', state.source_name)
    },
    setdbName(state, payload) {
      state.database_name = payload;
      console.log('database_name:', state.database_name)
    },
    setTableName(state, payload) {
      state.table_name = payload;
      console.log('table_name:', state.table_name)
    },
    setTableList(state, payload) {
      state.table_list = payload;
      console.log('table_list:', state.table_list)
    },
    setResource(state, payload) {
      state.resource = payload;
      console.log('resource:', state.resource)
    },
    setSaveName(state, payload) {
      state.save_name = payload;
      console.log('save_name:', state.save_name)
    },
   
    SET_LOGS_DATA(state, logsData) {
      state.logsData = logsData;
    },
  },
 

  actions: {
    // 调用 mutation 来更新用户数据的 action
    updateUserData({ commit }, logsData) {
      commit('SET_LOGS_DATA', logsData);
    },
  },
  getters: {
    // 获取用户数据的 getter
    getAllLogsData: (state) => state.logsData,
  },
});
export default store;
  // getters: {
  //   getDatabaseList(state) {
  //     const database_list = [];
  //     if (state.database_name && state.table_name && state.column_list.length) {
  //       database_list.push({
  //         database_name: state.database_name,
  //         table_list: [{
  //           table_name: state.table_name,
  //           column_list: state.column_list
  //         }]
  //       });
  //     }
  //     return database_list;
  //   }
  // },
