import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import VueRouter from 'vue-router'

import Home from '../views/home.vue'
import User from '../views/user/user.vue'
import Database from '../views/warehouse/database.vue'
import Roles from '../views/user/roles.vue'
import Main from '../views/main.vue'
import API from '../views/warehouse/api.vue'
import Register from '../views/login/register.vue'
import Operation from '../views/log/operation.vue'
import Schedul from '../views/log/schedul.vue'
import ApiSchedul from '../views/log/apischedul.vue'
import Detail from '../views/task/detail.vue'
import New from '../views/task/new.vue'
import Governance from '../views/log/governance.vue'
import Login from '../views/login/login.vue'
import echarts from 'echarts'
import Individual from '../views/user/individual.vue'
import Database1 from '../views/warehouse/database1.vue'
import 'element-ui/lib/theme-chalk/index.css';

//Vue.config.productionTip = false
Vue.use(ElementUI);

//路由守卫
Vue.use(VueRouter);
Vue.prototype.$echarts = echarts

//导入axios
import axios from 'axios'
Vue.prototype.$axios = axios

//Vuex状态管理
import store from './store' 

const routes = [
  {
    path: '/',
    component: Main,
    redirect: '/home',
    children: [
      {
        path: '/home',
        component: Home,
        meta: {
          requiresAuth: true, // 需要登录验证
        },
      },
      {
        path: '/user',
        component: User,
        meta: {
          requiresAuth: true
        }
      },
     /*  { path: '/roles', component: Roles }, */
     /*  { path: '/database', component: Database }, */
      { path: '/database1', component: Database1 },
      { path: '/api', component: API },
    /*   { path: '/register', component: Register }, */
      { path: '/operation', component: Operation },
      { path: '/schedul', component: Schedul },
      { path: '/apischedul', component: ApiSchedul },
      { path: '/detail', component: Detail },
     /*  { path: '/new', component: New }, */
      { path: '/governance', component: Governance },
      { path: '/individual', component: Individual },

    ]
  },
  {
    path: '/login',
    component: Login,
    meta: {
      requiresAuth: false,
    },
  },
]

const router = new VueRouter({
  routes
});

// 路由守卫
router.beforeEach((to, from, next) => {
  // 获取本地存储的 token
  const token = localStorage.getItem('token');

  // 如果访问的页面需要登录验证
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!token || token === 'null' || token === '') {
      // 未登录用户，重定向到登录页
      next('/login');
    } else {
      // 已登录用户或访问登录页，直接继续导航
      next();
    }
  } else {
    // 不需要登录验证的页面，直接继续导航到目标页面
    next();
  }
});

new Vue({
  store, // 2. 注入Vue实例
  router,
  render: (h) => h(App),
}).$mount('#app')
