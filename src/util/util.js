/**
 * 将给定的时间戳转换为日期字符串。
 *
 * @param {number} timestamp - 以毫秒为单位的时间戳。
 * @param {string} [format] - 输出的日期格式，默认为 "YYYY-MM-DD HH:mm:ss"。
 * @returns {string} 格式化后的日期字符串。
 */
  export default function timestampToDate(timestamp, format = 'YYYY-MM-DD HH:mm:ss') {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = `0${date.getMonth() + 1}`.slice(-2);
    const day = `0${date.getDate()}`.slice(-2);
    const hours = `0${date.getHours()}`.slice(-2);
    const minutes = `0${date.getMinutes()}`.slice(-2);
    const seconds = `0${date.getSeconds()}`.slice(-2);
  
    return format.replace('YYYY', year)
      .replace('MM', month)
      .replace('DD', day)
      .replace('HH', hours)
      .replace('mm', minutes)
      .replace('ss', seconds);
  }

//   /**
//  * 将给定的年月日时分秒转换为时间戳。
//  *
//  * @param {number} year - 年份。
//  * @param {number} month - 月份（1 到 12）。
//  * @param {number} day - 日（1 到 31）。
//  * @param {number} hours - 小时（0 到 23）。
//  * @param {number} minutes - 分钟（0 到 59）。
//  * @param {number} seconds - 秒（0 到 59）。
//  * @returns {number} 时间戳（毫秒为单位）。
//  */
// function dateToTimestamp(year, month, day, hours, minutes, seconds) {
//   const date = new Date(year, month - 1, day, hours, minutes, seconds);
//   return date.getTime();
// }

// export { dateToTimestamp };