// 配置devServer
const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  devServer: {
    host: '0.0.0.0',
    port: 80,  // 将端口改为80
    client: {
      webSocketURL: 'ws://0.0.0.0:80/ws',  // 也将WebSocket端口改为80
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    }
  },
  css: {
    extract: false,
    loaderOptions: {
      scss: {
        // Pass options to sass-loader
        // @/ is an alias to src/
        prependData: `@import "../src/css/custom-theme.css";`,
      },
    },
  },
});
