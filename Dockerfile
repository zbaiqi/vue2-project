# 使用基础镜像
FROM node:latest as build-stage

# 设置工作目录
WORKDIR /app

# 复制 package.json 和 package-lock.json（如果存在）到工作目录
COPY package*.json ./

# 安装依赖
RUN npm install

# 拷贝项目文件到工作目录
COPY . .

# 构建项目
RUN npm run build --prod

# 新的基础镜像，用于生产环境
FROM nginx:1.21

# 将构建好的文件复制到 Nginx 的默认静态文件目录
COPY --from=build-stage /app/dist /usr/share/nginx/html

# 暴露端口（如果需要）
EXPOSE 80

# 启动 Nginx 服务
CMD ["nginx", "-g", "daemon off;"]
